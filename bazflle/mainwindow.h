#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QDir>
#include <QStandardPaths>
#include <QString>
#include <QList>
#include <QListWidget>


#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE










class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_BtnHome_clicked();

    void on_BtnCredits_clicked();

    void on_BtnDownloads_clicked();

    void on_tabWidget_currentChanged(int index);

    void on_BtnPictures_clicked();

    void on_BtnDesktop_clicked();

    void on_BtnDocuments_clicked();

    void on_BtnVideos_clicked();

    void on_BtnTrash_clicked();
    //it's showing files on screen
    void  updateView(QDir directoryPath);
    //searching for path to go
    void PathSearch();

    void on_BtnSearchPath_clicked();

    void on_listWidget_itemClicked(QListWidgetItem *item);

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
