#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QStandardPaths>
#include <QMessageBox>
#include <QDir>
#include <QUrl>
#include <QStringList>
#include <GlobalVariablesAndFunctions.h>
#include <QDirIterator>
#include <QPushButton>
#include <QDebug>
#include <QFileInfo>
using namespace std;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateView(QDir directoryPath){
    ui->listWidget->clear();
    pathsList.clear();
    QDirIterator it(directoryPath);
    while (it.hasNext()) {
        pathsList.append(it.next());
      //  qDebug() << it.next();


        ui->listWidget->addItem(it.next());
    }

    for(int i =0; i<pathsList.count(); i++){

        qDebug() << pathsList[i];
    }
}
void MainWindow::PathSearch(){

}

void MainWindow::on_BtnHome_clicked()
{
 //actualPath = QStandardPaths::HomeLocation;
    //QDir directory(QDir::homePath());
    actualPath.setPath(QDir::homePath());
    if(!actualPath.exists()){
        QMessageBox warning;
        warning.setText("there is no such directory!!!");
        warning.exec();
    }
    else{
        if(ui->tabWidget->currentWidget()->objectName() == "tab")
        {
            ui->PathEdit1->setText(actualPath.path());
        }
        else{
            ui->PathEdit2->setText(actualPath.path());
        }


    }
   updateView(actualPath.path());


}
//CREDITS BUTTON CLICK
void MainWindow::on_BtnCredits_clicked()
{
    QMessageBox msgCredits;
    msgCredits.setText("Credits:\nbazfile made by Jakub Kołakowski and community.");
    msgCredits.setStandardButtons(QMessageBox::Ok);
    msgCredits.setDefaultButton(QMessageBox::Ok);
    msgCredits.exec();
}
//DOWNLOADS BUTTON CLICK
void MainWindow::on_BtnDownloads_clicked()
{

    //actualPath = QStandardPaths::HomeLocation;
    actualPath.setPath(downloadsFolder);
       if(!actualPath.exists()){
           QMessageBox warning;
           warning.setText("there is no such directory!!!");
           warning.exec();
       }
   else{
           if(ui->tabWidget->currentWidget()->objectName() == "tab")
           {
               ui->PathEdit1->setText(actualPath.path());
           }
           else{
               ui->PathEdit2->setText(actualPath.path());
           }

       }
       updateView(actualPath.path());
}
//TAB WIDGET BUTTON CLICK
void MainWindow::on_tabWidget_currentChanged(int index)
{
    if(ui->tabWidget->currentWidget()->objectName() == "tab")
    {
        actualPath.path() = ui->PathEdit1->toPlainText();
    }
    else{
        actualPath.path() = ui->PathEdit2->toPlainText();
    }
}
//PICTURES BUTTON CLICK
void MainWindow::on_BtnPictures_clicked()
{
    actualPath.setPath(picturesFolder);
    if(!actualPath.exists()){
        QMessageBox warning;
        warning.setText("there is no such directory!!!");
        warning.exec();
    }
    else{
        if(ui->tabWidget->currentWidget()->objectName() == "tab")
        {
            ui->PathEdit1->setText(actualPath.path());
        }
        else{
            ui->PathEdit2->setText(actualPath.path());
        }


    }
    updateView(actualPath.path());
}


void MainWindow::on_pushButton_clicked()
{


}
//DESKTOP BUTTON CLICK
void MainWindow::on_BtnDesktop_clicked()
{
    actualPath.setPath(desktopFolder);
    if(!actualPath.exists()){
        QMessageBox warning;
        warning.setText("there is no such directory!!!");
        warning.exec();
    }
    else{
        if(ui->tabWidget->currentWidget()->objectName() == "tab")
        {
            ui->PathEdit1->setText(actualPath.path());
        }
        else{
            ui->PathEdit2->setText(actualPath.path());
        }
       }
    updateView(actualPath.path());
}
//DOCUMENTS BUTTON CLICK
void MainWindow::on_BtnDocuments_clicked()
{
    actualPath.setPath(documentsFolder);
    if(!actualPath.exists()){
        QMessageBox warning;
        warning.setText("there is no such directory!!!");
        warning.exec();
    }
    else{
        if(ui->tabWidget->currentWidget()->objectName() == "tab")
        {
            ui->PathEdit1->setText(actualPath.path());
        }
        else{
            ui->PathEdit2->setText(actualPath.path());
        }
       }
}

void MainWindow::on_BtnVideos_clicked()
{
    actualPath.setPath(videosFolder);
    if(!actualPath.exists()){
        QMessageBox warning;
        warning.setText("there is no such directory!!!");
        warning.exec();
    }
    else{
        if(ui->tabWidget->currentWidget()->objectName() == "tab")
        {
            ui->PathEdit1->setText(actualPath.path());
        }
        else{
            ui->PathEdit2->setText(actualPath.path());
        }
       }
}

void MainWindow::on_BtnTrash_clicked()
{
    actualPath.setPath(trashFolder);
    if(!actualPath.exists()){
        QMessageBox warning;
        warning.setText("there is no such directory!!!");
        warning.exec();
    }
    else{
        if(ui->tabWidget->currentWidget()->objectName() == "tab")
        {
            ui->PathEdit1->setText(actualPath.path());
        }
        else{
            ui->PathEdit2->setText(actualPath.path());
        }
       }
}
//Search path button
void MainWindow::on_BtnSearchPath_clicked()
{
    updateView(ui->PathEdit1->toPlainText());
}

void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
       QFileInfo file(item->text());
         QString fileExtension = file.suffix();
         qDebug() << fileExtension;
        QString itemPath = item->text();
         if(fileExtension == "")
         {

             actualPath.path() = itemPath;
             qDebug() << actualPath.path();


         }





}




