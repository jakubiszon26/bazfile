#ifndef GLOBALVARIABLES_H
#define GLOBALVARIABLES_H
#include "ui_mainwindow.h"
#include "mainwindow.h"
#include <QDir>
#include <QStandardPaths>
#include <QMessageBox>

//actual user path variable
static QDir actualPath;
static QDir previoustPath;
//path to user home folder
static const QString homeFolder = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
//path to user downloads folder
static const QString downloadsFolder = QStandardPaths::writableLocation(QStandardPaths::DownloadLocation);
//path to user pictures folder
static const QString picturesFolder = QStandardPaths::writableLocation(QStandardPaths::PicturesLocation);
//path to user documents folder
static const QString documentsFolder = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
//path to user desktop folder
static const QString desktopFolder = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
//path to user videos folder
static const QString videosFolder = QStandardPaths::writableLocation(QStandardPaths::MoviesLocation);
//fake trash (because i wasn't able to get acces in to real trash directory so i crated one xD
static const QString trashFolder = "~/Trash";
//paths list
static QList <QString> pathsList;











#endif // GLOBALVARIABLES_H
