***BAZFILE FILE MANAGER***

bazfile will be  high customizable file manager witch a lot of functions for linux and windows(soon).

You can help me in this project if you want because it's full opensource ;)

***how it looks for now?***

![Image of look](look.png)

***how to install on linux***

1. Create a folder where you gonna clone this repository
```bash
mkdir apps && cd apps
```
2. Clone repository and cd in to it
```bash
git clone https://gitlab.com/jakubiszon26/bazfile.git && cd bazfile
```
3. get in to bazflle folder
```bash
cd bazflle
```
4. Run qmake
```bash
qmake bazffle.pro
```
5. Create executable
```
make
```
6. Run bazfile
```bash
./bazflle
```
Should work




